import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { EmailComposer } from '@ionic-native/email-composer';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Page1 } from '../pages/page1/page1';
import { Page2 } from '../pages/page2/page2';
import { EmergenciesPage } from '../pages/emergencies/emergencies';
import { SpeakToMajorPage } from '../pages/speak-to-major/speak-to-major';

import { LoginPage } from '../pages/login/login';
import { User } from '../providers/user';
import { ApiService } from '../providers/api-service';


@Component({
    templateUrl: 'app.html'
})
export class MyApp {

    private active_page: string = "#e3e5e8";
    private unactive_page: string = "white";

    public username: String;
    public name: String;

    public total_complaints: number;

    @ViewChild(Nav) nav: Nav;
    rootPage: any = LoginPage;
    pages: Array<{ title: string, icon: string, component: any, active: string }>;

    constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,
        private emailComposer: EmailComposer, public menuCtrl: MenuController, private apiService: ApiService) {
        this.initializeApp();
        this.pages = [
            { title: 'Denuncias', icon: 'filing', component: Page1, active: this.active_page },
            { title: 'Nosotros', icon: 'home', component: Page2, active: this.unactive_page },
            { title: 'Emergencias', icon: 'call', component: EmergenciesPage, active: this.unactive_page },
            { title: 'Escribele al Alcalde', icon: 'clipboard', component: SpeakToMajorPage, active: this.unactive_page },
        ];
    }

    initializeApp() {
        this.platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            this.statusBar.backgroundColorByHexString("#518e39");
            //this.statusBar.overlaysWebView(false);
            setTimeout(() => {
                this.splashScreen.hide();
            }, 100);

            this.platform.registerBackButtonAction(() => {
                if (this.menuCtrl.isOpen()) {
                    this.menuCtrl.close();
                }
                else if (this.nav.getActive().component.name == "Page2" ||
                    this.nav.getActive().component.name == "EmergenciesPage" ||
                    this.nav.getActive().component.name == "SpeakToMajorPage") {

                    for (var p of this.pages)
                        if (p.component.name == this.nav.getActive().component.name)
                            p.active = this.unactive_page;

                    this.pages[0].active = this.active_page;
                    this.nav.pop();
                    this.nav.setRoot(Page1);
                } else if (this.nav.getActive().component.name == "Page1") {
                    this.platform.exitApp();
                } else {
                    this.nav.pop();
                }


            }, 500);
        });
    }

    openPage(page) {
        for (var p of this.pages)
            if (p.component.name == this.nav.getActive().component.name)
                p.active = this.unactive_page;

        this.nav.setRoot(page.component);
        page.active = this.active_page;
    }


    signOut() {
        this.apiService.logoutUser();
        this.pages[0].active = this.active_page;
        this.pages[1].active = this.unactive_page;
        this.pages[2].active = this.unactive_page;
        this.pages[3].active = this.unactive_page;
        this.nav.setRoot(LoginPage);

    }

    helpAndFeedback() {
        this.emailComposer.isAvailable().then((available: boolean) => {
            if (available) {
                //Now we know we can send
            }
        });

        let email = {
            to: 'alcaldia@asde.gov.do',
            cc: '',
            bcc: ['', ''],
            attachments: [],
            subject: 'Ayuda o Retroalimentaci�n',
            body: '',
            isHtml: true
        };

        // Send a text message using default options
        this.emailComposer.open(email);
    }

}
