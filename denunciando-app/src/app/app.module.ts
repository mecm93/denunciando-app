import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { IonicStorageModule } from '@ionic/storage';

//PAGES
import { Page1 } from '../pages/page1/page1';
import { Page2 } from '../pages/page2/page2';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { ForgotPwPage } from '../pages/forgot-pw/forgot-pw';
import { TermsPage } from '../pages/terms/terms';
import { ComplaintPage } from '../pages/complaint/complaint';
import { EmergenciesPage } from '../pages/emergencies/emergencies';
import { SpeakToMajorPage } from '../pages/speak-to-major/speak-to-major';
import { ComplaintConfirmationPage } from '../pages/complaint-confirmation/complaint-confirmation';
import { ComplaintReviewPage } from '../pages/complaint-review/complaint-review';
import { MapPage } from '../pages/map/map';

//PROVIDERS & DIRECTIVES
import { User } from '../providers/user';
import { Complaint } from '../providers/complaint';
import { Complaints } from '../providers/complaints';
import { Message } from '../providers/message';
import { Map } from '../providers/map';
import { ApiService } from '../providers/api-service';
import { GlobalData } from '../providers/global-data';
import { Elastic } from '../components/elastic/elastic';

//IONIC NATIVE 
import { Geolocation } from '@ionic-native/geolocation';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Camera } from '@ionic-native/camera';
import { EmailComposer } from '@ionic-native/email-composer';
import { MediaPlugin } from '@ionic-native/media';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { GoogleMaps } from '@ionic-native/google-maps';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { MediaCapture } from '@ionic-native/media-capture';
import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';

@NgModule({
    declarations: [
        MyApp,
        Page1,
        Page2,
        LoginPage,
        RegisterPage,
        ForgotPwPage,
        TermsPage,
        EmergenciesPage,
        SpeakToMajorPage,
        ComplaintPage,
        ComplaintConfirmationPage,
        ComplaintReviewPage,
        MapPage,
        Elastic
    ],
    imports: [
        IonicModule.forRoot(MyApp, {
            platforms: {
                ios: {
                    backButtonText: 'Atras',
                }
            }
        }),
        IonicStorageModule.forRoot()
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        Page1,
        Page2,
        LoginPage,
        RegisterPage,
        ForgotPwPage,
        TermsPage,
        ComplaintPage,
        EmergenciesPage,
        SpeakToMajorPage,
        ComplaintConfirmationPage,
        ComplaintReviewPage,
        MapPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        Camera,
        EmailComposer,
        User,
        Geolocation,
        Complaint,
        Complaints,
        MediaPlugin,
        LocationAccuracy,
        GoogleMaps,
        Message,
        InAppBrowser,
        Map,
        MediaCapture,
        ApiService,
        GlobalData,
        File,
        Transfer,
        FilePath,
        { provide: ErrorHandler, useClass: IonicErrorHandler }
    ]
})
export class AppModule { }
