import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';

import { Page1 } from '../page1/page1';
import { Complaint } from '../../providers/complaint';
import { Complaints } from '../../providers/complaints';
import { Message } from '../../providers/message';
import { ApiService } from '../../providers/api-service';

/*
  Generated class for the ComplaintConfirmation page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

declare var navigator: any;
declare var Connection: any;
declare var google;

@Component({
  selector: 'page-complaint-confirmation',
  templateUrl: 'complaint-confirmation.html'
})
export class ComplaintConfirmationPage {

    public complaint: Complaint = new Complaint();
    //private complaints: Complaints = new Complaints();
    public video_index = 0;
    public audio_index = 0;
    @ViewChild('map') mapElement: ElementRef;

    constructor(public navCtrl: NavController, public navParams: NavParams, private message: Message,
        private apiService: ApiService, private menuCtrl: MenuController) {
        this.menuCtrl.get("side-menu").enable(false);
        this.menuCtrl.get("side-menu").swipeEnable(false);

        this.complaint = new Complaint();

        let main_data = this.navParams.data.complaint;
        let map_data = this.navParams.data.map_complaint;
        
        this.complaint.setComplaintTypeIndex(main_data.complaint_type_index);
        this.complaint.setComplaintTypeName(main_data.complaint_type_name);
        this.complaint.setComplaintDescription(main_data.complaint_description);
        this.complaint.setComplaintVideo(main_data.complaint_video);
        this.complaint.setComplaintAudio(main_data.complaint_audio);
        this.complaint.setComplaintPhotos(main_data.complaint_photos);
        this.complaint.setComplaintPhotoIndex(main_data.complaint_photos_index);

        this.complaint.setComplaintMapAddress(map_data.complaint_map_address);
        this.complaint.setComplaintMapLatLng(map_data.complaint_map_latLng);
        this.complaint.setComplaintMapLat(this.complaint.getComplaintMapLatLng().lat());
        this.complaint.setComplaintMapLng(this.complaint.getComplaintMapLatLng().lng());
        if (this.complaint.getComplaintVideo() !== null)
            ++this.video_index;
        if (this.complaint.getComplaintAudio() !== null)
            ++this.audio_index;

    }

    ionViewDidLoad() {
        this.loadMap();
    }

    loadMap() {
        let lat = this.complaint.getComplaintMapLatLng().lat();
        let lng = this.complaint.getComplaintMapLatLng().lng();
        let latLng = new google.maps.LatLng(lat, lng);

        let mapOptions = {
            center: latLng,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true
        }
        var map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

        var marker = new google.maps.Marker({
            map: map,
            position: latLng
        });

    }

    sendComplaint() {
        this.apiService.createComplaint(this.complaint).subscribe(
            data => {
                this.message.showToast('Denuncia recibida, estamos trabajando. #' + data.id);
                this.complaint.setComplaintId(data.id);
            }, error => {
                // this.message.showAlert("SEND: " + JSON.stringify(error));
            }, () => {
                this.navCtrl.setRoot(Page1);
                this.apiService.addAttachments(this.complaint);
            }
        );
  }

}
