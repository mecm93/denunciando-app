import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Complaint } from '../../providers/complaint';

/*
  Generated class for the ComplaintReview page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

declare var google;

@Component({
  selector: 'page-complaint-review',
  templateUrl: 'complaint-review.html'
})
export class ComplaintReviewPage {

    public complaint: Complaint;
    public video_index = 0;
    public audio_index = 0;
    @ViewChild('map') mapElement: ElementRef;

    constructor(public navCtrl: NavController, public navParams: NavParams) {
        this.complaint = navParams.data.complaint;
        console.log(JSON.stringify(this.complaint));
        if (this.complaint.getComplaintVideo() !== null)
            ++this.video_index;
        if (this.complaint.getComplaintAudio() !== null)
            ++this.audio_index;
    }


    ionViewDidLoad() {
        this.loadMap();
    }

    loadMap() {
        let lat = this.complaint.getComplaintMapLat();
        let lng = this.complaint.getComplaintMapLng();
        let latLng = new google.maps.LatLng(lat, lng);

        let mapOptions = {
            center: latLng,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true
        }
        var map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

        var marker = new google.maps.Marker({
            map: map,
            position: latLng
        });

    }

}
