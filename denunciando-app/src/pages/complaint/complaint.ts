import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, FabContainer, MenuController, Platform } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { DomSanitizer } from '@angular/platform-browser';
import { MediaPlugin } from '@ionic-native/media';
import { MediaCapture, MediaFile } from '@ionic-native/media-capture';

import { ComplaintConfirmationPage } from '../complaint-confirmation/complaint-confirmation';
import { MapPage } from '../map/map';
import { Complaint } from '../../providers/complaint';

import { Message } from '../../providers/message';

import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';

declare var cordova: any;

/*
  Generated class for the Complaint page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

declare var navigator: any;
declare var Connection: any;

@Component({
    selector: 'page-complaint',
    templateUrl: 'complaint.html'
})
export class ComplaintPage {

    //PAGE DATA
    complaint_types: Array<{ title: string, value: number }>;

    //CAMERA
    public photoTaken: boolean[];
    private IMAGE: string = "IMAGE";

    //VIDEO
    public videoTaken: boolean;
    private VIDEO: string = "VIDEO";

    //AUDIO
    public audioTaken: boolean;
    private AUDIO: string = "AUDIO";

    //COMPLAINT
    //TODO: Remove static, not necessary; Workaround
    private static complaint: Complaint;

    constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
        public _DomSanitizationService: DomSanitizer, public camera: Camera, public media: MediaPlugin,
        public message: Message, private mediaCapture: MediaCapture, private menuCtrl: MenuController,
        private file: File, private filePath: FilePath, private platform: Platform) {

        this.menuCtrl.get("side-menu").enable(false);
        this.menuCtrl.get("side-menu").swipeEnable(false);

        this.photoTaken = [false, false, false];
        this.videoTaken = false;
        this.audioTaken = false;

        this.complaint_types = [
            { title: 'Vertedero', value: 1 },
            { title: 'No Pasa el Cami\xF3n', value: 2 },
            { title: 'Parque Descu\xEDdado', value: 3 },
            { title: 'Calles no Asfaltadas', value: 4 },
            { title: 'Escombros', value: 5 },
            { title: 'Alumbrado de Parque', value: 6 },
            { title: 'Drenaje Tapada', value: 7 },
            { title: 'Sem\xE1foro Da\xF1ado', value: 8 },
            { title: 'Arboles Ca\xEDdos', value: 9 },
            { title: 'Otros', value: 10 }
        ];

        ComplaintPage.complaint = new Complaint();
    }

    onContentClick(fab: FabContainer) {
        fab.close();
    }

    getCameraIndex(): number {
        return ComplaintPage.complaint.getComplaintPhotoIndex();
    }

    getComplaintPhotoUrl(index: number): string {
        if (index < this.getCameraIndex())
            return ComplaintPage.complaint.getComplaintPhotoByIndex(index).substring(ComplaintPage.complaint.getComplaintPhotoByIndex(index).lastIndexOf('/') + 1);

        return null;
    }
    getComplaintAddress(): string {
        return ComplaintPage.complaint.getComplaintMapAddress();
    }


    getComplaintVideoUrl(): string {
        return ComplaintPage.complaint.getComplaintVideo();
    }

    getDescriptionLength(text: string): number {
        if (text !== undefined)
            return 200 - text.length;
        else
            return 200;
    }

    getVideoName(): string {
        return ComplaintPage.complaint.getComplaintVideo();
    }

    getAudioName(): string {
        return ComplaintPage.complaint.getComplaintAudio();
    }

    removePhotoByIndex(index: number) {
        // Shift data to corresponding indexes depending on index number
        ComplaintPage.complaint.removeComplaintPhotoByIndex(index);
        // TODO: currently, if I have 3 images, and I try to delete the first one, data does not shift. And I am not able to add another image.
        switch (index) {
            case 0:
                if ((this.photoTaken[1]) && (this.photoTaken[2])) {
                    this.photoTaken[2] = false;
                } else if (this.photoTaken[1]) {
                    this.photoTaken[1] = false;
                } else {
                    this.photoTaken[0] = false;
                }
                break;
            case 1:
                if (this.photoTaken[2]) {
                    this.photoTaken[2] = false;
                } else {
                    this.photoTaken[1] = false;
                }
                break;
            case 2:
                this.photoTaken[index] = false;
                break;
        }
    }

    removeVideo() {
        ComplaintPage.complaint.setComplaintVideo(null);
        this.videoTaken = false;
    }

    removeAudio() {
        ComplaintPage.complaint.setComplaintAudio(null);
        this.audioTaken = false;
    }

    openMap(type?: number, another?: string, description?: string) {
        if (type === undefined)
            this.message.showAlert('Debe seleccionar el Tipo de Denuncia');
        else {
            ComplaintPage.complaint.setComplaintTypeIndex(type);

            if (this.complaint_types[type - 1].title === "Otros") {
                if (another === undefined || another.trim().length < 1)
                    this.message.showAlert('Debe de espec\xEDficar la denuncia');
                else
                    ComplaintPage.complaint.setComplaintTypeName(another);
            }
            else
                ComplaintPage.complaint.setComplaintTypeName(this.complaint_types[type - 1].title);

            if (description === undefined)
                this.message.showAlert('Debe agregar una peque\xF1a descripci\xF3n');
            else
                ComplaintPage.complaint.setComplaintDescription(description);

            if (ComplaintPage.complaint.getComplaintTypeName() !== undefined && ComplaintPage.complaint.getComplaintDescription() !== undefined)
                this.navCtrl.push(MapPage, { complaint: ComplaintPage.complaint });

        }
    }


    // =====================================================================================
    //                              NATIVE EVENTS
    // =====================================================================================

    selectPicFromGallery(fab?: FabContainer) {
        var options = {
            correctOrientation: true,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
        };
        this.camera.getPicture(options).then((imageData) => {
            if (this.platform.is('android')) {
                this.filePath.resolveNativePath(imageData)
                    .then(filePath => {
                        let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                        let currentName = imageData.substring(imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
                        this.copyFileToLocalDir(this.IMAGE, correctPath, currentName, this.createFileName(this.IMAGE));
                    });
            } else {
                var currentName = imageData.substr(imageData.lastIndexOf('/') + 1);
                var correctPath = imageData.substr(0, imageData.lastIndexOf('/') + 1);
                this.copyFileToLocalDir(this.IMAGE, correctPath, currentName, this.createFileName(this.IMAGE));
            }


        }, (err) => {
        });

        fab.close();
    }

    openCamera(fab?: FabContainer) {

        var options = {
            correctOrientation: true,
            sourceType: this.camera.PictureSourceType.CAMERA
        };
        this.camera.getPicture(options).then((imageData) => {
            var currentName = imageData.substr(imageData.lastIndexOf('/') + 1);
            var correctPath = imageData.substr(0, imageData.lastIndexOf('/') + 1);
            this.copyFileToLocalDir(this.IMAGE, correctPath, currentName, this.createFileName(this.IMAGE));
        }, (err) => {
        });

        fab.close();
    }

    selectVideoFromGallery(fab?: FabContainer) {
        var options = {
            correctOrientation: true,
            mediaType: this.camera.MediaType.VIDEO,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
        }

        this.camera.getPicture(options).then((videoData) => {
            if (this.platform.is('android')) {
                this.filePath.resolveNativePath('file://' + videoData)
                    .then(filePath => {
                        let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                        let currentName = filePath.substring(filePath.lastIndexOf('/') + 1);
                        this.copyFileToLocalDir(this.VIDEO, correctPath, currentName, this.createFileName(this.VIDEO));
                    }, err => {
                    });
            } else {
                var currentName = videoData.substr(videoData.lastIndexOf('/') + 1);
                var correctPath = videoData.substr(0, videoData.lastIndexOf('/') + 1);
                this.copyFileToLocalDir(this.VIDEO, correctPath, currentName, this.createFileName(this.VIDEO));
            }
        }, (err) => {
        });

        fab.close();
    }

    openVideoCamera(fab?: FabContainer) {
        var options = {
            limit: 1,
            duration: 20
        }
        this.mediaCapture.captureVideo(options)
            .then((videoData: MediaFile[]) => {
                var currentName = videoData[0].fullPath.substr(videoData[0].fullPath.lastIndexOf('/') + 1);
                var correctPath = videoData[0].fullPath.substr(0, videoData[0].fullPath.lastIndexOf('/') + 1);
                this.copyFileToLocalDir(this.VIDEO, correctPath, currentName, this.createFileName(this.VIDEO));
            }, (err) => {
            });
        fab.close();
    }


    openRecorder(fab?: FabContainer) {
        var options = {
            limit: 1,
            duration: 40
        }

        this.mediaCapture.captureAudio(options)
            .then((audioData: MediaFile[]) => {
                var currentName = audioData[0].fullPath.substr(audioData[0].fullPath.lastIndexOf('/') + 1);
                var correctPath = audioData[0].fullPath.substr(0, audioData[0].fullPath.lastIndexOf('/') + 1);
                this.copyFileToLocalDir(this.AUDIO, correctPath, currentName, this.createFileName(this.AUDIO));
            }, (err) => {
            });

        fab.close();
    }

    //=======================================================================



    //=======================================================================
    //                            HANDLERS
    //=======================================================================

    private createFileName(type: string) {
        var ext = (type == this.IMAGE) ? ".jpg" : (type == this.VIDEO) ? ".mp4" : (type == this.AUDIO) ? ".m4a" : null;
        var d = new Date(),
            n = d.getTime(),
            newFileName = n + ext;
        return newFileName;
    }

    private copyFileToLocalDir(type: string, namePath, currentName, newFileName) {
        this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
            if (type == this.IMAGE) {
                ComplaintPage.complaint.addComplaintPhoto(newFileName);
                this.photoTaken[this.getCameraIndex() - 1] = true;
            } else if (type == this.VIDEO) {
                ComplaintPage.complaint.setComplaintVideo(newFileName);
                this.videoTaken = true;
            } else if (type == this.AUDIO) {
                ComplaintPage.complaint.setComplaintAudio(newFileName);
                this.audioTaken = true;
            }
        }, error => {
            let path = namePath.replace("file:///storage/emulated/0/", "");
            if (type == this.AUDIO) {
                ComplaintPage.complaint.setComplaintAudio(path + currentName);
                this.audioTaken = true;
            } else if (type == this.VIDEO) {
                // TODO: Check currentName, on some cases it is not working properly
                // let path = namePath.replace("//storage/emulated/0/", "");
                // this.message.showAlert("VIDEO " + path + currentName);
                ComplaintPage.complaint.setComplaintVideo(path + currentName);
                this.videoTaken = true;
            } else
                this.message.showAlert('Error guardando la ' + type);
        });
    }

    //=======================================================================



}
