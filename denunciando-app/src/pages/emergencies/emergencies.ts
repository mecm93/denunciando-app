import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Page1 } from '../page1/page1';
/*
  Generated class for the Emergencies page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'page-emergencies',
    templateUrl: 'emergencies.html'
})
export class EmergenciesPage {

    list: Array<{ name: string, image: string, tel: string }>;

    constructor(public navCtrl: NavController, public navParams: NavParams) {
        this.list = [
            { name: 'Cuerpo de Bomberos Santo Domingo Este', image: 'img/firefighters.jpg', tel: '809-695-3164' },
            { name: 'Universidad Aut\xF3noma de Santo Domingo (UASD)', image: 'img/uasd.jpg', tel: '809-597-0018' },
            { name: 'Cruz Roja Dominicana', image: 'img/hospital.jpg', tel: '809-238-5312' },
            { name: 'Autoridad Metropolitana de Transporte (AMET)', image: 'img/amet.jpg', tel: '809-686-6520' },
            { name: 'Cementerio Cristo Salvador', image: 'img/graveyard.png', tel: '809-937-0842' },
            { name: 'Funeraria Municipal Villa Carmen', image: 'img/symbol.png', tel: '809-728-2762' },
            { name: 'Funeraria Municipal Isabelita', image: 'img/symbol.png', tel: '809-599-2798' },
            { name: 'Drenaje Pluvial ASDE', image: 'img/symbol.png', tel: '809-788-7676 ext.2852' },
            { name: 'Ingenier\xEDa y Obras ASDE', image: 'img/symbol.png', tel: '809-788-7676 ext.2854' },
            { name: 'LIMPIA ASDE', image: 'img/symbol.png', tel: '809-788-7676 ext.2521' }
        ];
    }

    ionViewDidLoad() { }
}
