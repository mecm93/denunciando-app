import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { Message } from '../../providers/message';
import { ApiService } from '../../providers/api-service';

/*
  Generated class for the ForgotPw page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'page-forgot-pw',
    templateUrl: 'forgot-pw.html'
})
export class ForgotPwPage {

    constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
        public message: Message, public apiService: ApiService, public loadingCtrl: LoadingController) { }

    ionViewDidLoad() { }

    pwRecovery(email) {
        let loader = this.loadingCtrl.create({
            content: "Procesando..."
        });
        loader.present();
        this.apiService.recoverPassword(email).subscribe(
            data => {
				console.log("DATA" + JSON.stringify(data));
			},
            error => {
                this.message.showAlert('El correo ingresado no se encuentra registrado o hubo un error ejecutando la busqueda; Por favor intente luego.');
                console.log("ERROR" + JSON.stringify(error) + error);
                loader.dismiss();
            },
            () => {
                this.message.showToast('Se ha enviado un correo con sus credenciales');
                this.navCtrl.pop();
                loader.dismiss();
            }
        );
    }

}
