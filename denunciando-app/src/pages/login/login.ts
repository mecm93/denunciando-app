import { Component } from '@angular/core';
import { LoadingController, NavController, MenuController } from 'ionic-angular';
import { MyApp } from '../.././app/app.component';

import { Page1 } from '../page1/page1';
import { RegisterPage } from '../register/register';
import { ForgotPwPage } from '../forgot-pw/forgot-pw';
import { ApiService } from '../../providers/api-service';
import { Message } from '../../providers/message';
import { Storage } from '@ionic/storage';

/*
  Generated class for the Login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'page-login',
    templateUrl: 'login.html'
})
export class LoginPage {
    constructor(public loadingCtrl: LoadingController, public navCtrl: NavController,
        public menuCtrl: MenuController, public apiService: ApiService, public message: Message, private storage: Storage) {
        this.menuCtrl.get("side-menu").enable(false);
        this.menuCtrl.get("side-menu").swipeEnable(false);
    }


    signIn(event, email, password) {

        let loader = this.loadingCtrl.create({
            content: "Ingresando..."
        });
        loader.present();


        setTimeout(() => {
            this.apiService.loginUser(email, password).subscribe(
                response => {
                    console.log(JSON.stringify(response));
                    if (response.activo) {
                        this.apiService.setCurrentUser(response);
                        this.navCtrl.setRoot(Page1);
                    }
                    else
                        this.message.showCustomAlert('Usuario Inv&#225;lido', 'A&#250;n no ha validado su cuenta; Ingrese a su correo y siga los pasos indicados.');
                }, err => {
                    this.message.showCustomAlert('Advertencia', 'Hubo un error con su correo o contrase&#241;a; Por favor verificar e intentar luego.');
                    loader.dismissAll();
                }, () => {
                    loader.dismissAll();
                }
            );
        }, 750);
    }

    register() {
        this.navCtrl.push(RegisterPage);
    }

    forgotPw() {
        this.navCtrl.push(ForgotPwPage);
    }
}
