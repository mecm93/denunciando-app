import { Component, ViewChild, ElementRef, NgZone} from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import { Geolocation, Geoposition  } from '@ionic-native/geolocation';

import { ComplaintConfirmationPage } from '../complaint-confirmation/complaint-confirmation';
import { Complaint } from '../../providers/complaint';
import { Message } from '../../providers/message';
import { Map } from '../../providers/map';

/*
  Generated class for the Map page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

declare var google;
declare var klokantech;
declare var navigator;
declare var plugin;

@Component({
  selector: 'page-map',
  templateUrl: 'map.html'
})
export class MapPage {
    
    private complaint: Complaint;
    private pre_complaint: Complaint;

    //DEFAULT VALUES TO CENTER MAP
    private center_lat: number = 18.505439;
    private center_lng: number = -69.811439;
    
    public marker_visible: boolean;
    public pre_marker: any;

    @ViewChild('map') mapElement: ElementRef;

    constructor(public navCtrl: NavController, public navParams: NavParams, public message: Message,
        public geolocation: Geolocation, public zone: NgZone, public map: Map, private menuCtrl: MenuController) {
        this.menuCtrl.get("side-menu").enable(false);
        this.menuCtrl.get("side-menu").swipeEnable(false);
        this.pre_complaint = this.navParams.data.complaint;
        this.marker_visible = false;
        this.complaint = new Complaint();
    }

    ionViewDidLoad() {
        this.loadMap();
    }
    
  loadMap() {
      let latLng = new google.maps.LatLng(this.center_lat, this.center_lng);

      let mapOptions = {
          center: latLng,
          tilt: 2,
          zoom: 12,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          disableDefaultUI: true
      }


      //centerControlDiv.index = 1;
      
      var map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

      /*var geolocationDiv = document.createElement('div');
      var geolocationCtrl = this.GeolocationControl(geolocationDiv, map);
      //var geoloccontrol = new klokantech.GeolocationControl(map, 12);
      map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(geolocationDiv);*/
      this.addMarker(map);
      this.geolocate(map);
  }

  addMarker(map): any {
      google.maps.event.addListener(map, 'click', (event) => {
          var found = false;
          var latLng = event.latLng;
          var geocoder = new google.maps.Geocoder;
          geocoder.geocode({ 'location': event.latLng }, (results, status) => {
              for (let i of [1, 2, 3, 4, 5, 6, 7, 8, 9])
                  if (results[0] !== undefined &&
                      results[0].address_components[i] !== undefined &&
                      results[0].address_components[i].long_name !== undefined)
                      if (results[0].address_components[i].long_name == "Santo Domingo Este") {
                          if (status === 'OK') {
                              found = true;
                              break;
                          }
                      }


              //VALIDATION ON THIS
              console.log(results[0].formatted_address);
              if (!found)
                  this.message.showAlert("La ubicaci\xF3n esta fuera de Santo Domingo Este");
              else {
                  if (this.marker_visible) {
                      //Remove previous marker from map
                      this.pre_marker.setMap(null);
                  }

                  var marker = new google.maps.Marker({
                      map: map,
                      position: latLng
                  });

                  this.zone.run(() => {
                      this.pre_marker = marker;
                      this.marker_visible = true;
                      this.complaint.setComplaintMapLatLng(latLng);
                      this.complaint.setComplaintMapAddress(results[0].formatted_address.replace("Dominican Republic", "Republica Dominicana"));
                      
                  });

              }
          });

      });
  }

  private GeolocationControl(controlDiv, map) {

    // Set CSS for the control border.
    var controlUI = document.createElement('button');
    controlUI.style.backgroundColor = '#fff';
    controlUI.style.border = '2px solid #fff';
    controlUI.style.borderRadius = '3px';
    controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
    controlUI.style.cursor = 'pointer';
    controlUI.style.marginBottom = '22px';
    controlUI.style.textAlign = 'center';
    controlUI.title = 'Click to go to my location';
    controlDiv.appendChild(controlUI);

    // Set CSS for the control interior.
    var controlText = document.createElement('div');
    controlText.style.color = 'rgb(25,25,25)';
    controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
    controlText.style.fontSize = '16px';
    controlText.style.lineHeight = '38px';
    controlText.style.paddingLeft = '5px';
    //controlText.style.paddingRight = '5px';
        // ADD ICON OF GEO
    controlText.innerHTML = 'Mi Localizaci\xF3n';
    controlUI.appendChild(controlText);

    google.maps.event.addDomListener(controlUI, 'click', this.geolocate(map));

  }

    geolocate(map) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => {
                var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude)
                map.setZoom(16);
                map.setCenter(pos);
            });
        }
    }

  confirmComplaint() {
      this.navCtrl.push(ComplaintConfirmationPage, { complaint: this.pre_complaint, map_complaint: this.complaint });
  }

  
}
