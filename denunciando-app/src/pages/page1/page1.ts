import { Component, NgZone } from '@angular/core';

import { NavController, ModalController, NavParams, MenuController, LoadingController } from 'ionic-angular';
import { MyApp } from '../.././app/app.component';
import { ComplaintPage } from '../complaint/complaint';
import { ComplaintReviewPage } from '../complaint-review/complaint-review';
import { Complaint } from '../../providers/complaint';
import { Complaints } from '../../providers/complaints';
import { ApiService } from '../../providers/api-service';
import { Message } from '../../providers/message';

@Component({
    selector: 'page-page1',
    templateUrl: 'page1.html'
})
export class Page1 {

    // TODO
    // FETCH DATA AND UPDATE IF DATA HAS CHANGED
    // REMOVE UNNCESSARY IMPORTS 

    public user: any;
    public list_of_complaints: Array<Complaint> = [];
    private IMAGE: string = "IMAGE";
    private AUDIO: string = "AUDIO";
    private VIDEO: string = "VIDEO";

    constructor(public navCtrl: NavController, public modalCtrl: ModalController, public navParams: NavParams,
        public menuCtrl: MenuController, private apiService: ApiService, private zone: NgZone, private loadingCtrl: LoadingController, private message: Message) {


        this.user = this.apiService.getCurrentUser();
        MyApp.prototype.username = this.user.getUserEmail();
        MyApp.prototype.name = this.user.getUserFullName();

    }

    ionViewWillEnter() {
        this.menuCtrl.get("side-menu").enable(true);
        this.menuCtrl.get("side-menu").swipeEnable(true);
        this.zone.run(() => {
            this.setListOfComplaints();
        });
    }

    doRefresh(refresher) {
        setTimeout(() => {
            this.setListOfComplaints();
            refresher.complete();
        }, 2000);
    }

    setListOfComplaints() {
        var tmp_list_of_complaints = [];

        let loader = this.loadingCtrl.create({
            content: "Actualizando..."
        });
        loader.present();

        this.apiService.getComplaints().subscribe(
            data => {
                //Validate data
                if (data && data.length > 0)
                    for (let item of data) {
                        let comp = new Complaint();
                        comp.setComplaintId(item.id);
                        comp.setComplaintMapAddress(item.direccion);
                        comp.setComplaintDescription(item.descripcion);
                        comp.setComplaintDate(item.fecha);
                        comp.setComplaintMapLat(item.latitud);
                        comp.setComplaintMapLng(item.longitud);
                        comp.setComplaintTypeIndex(item.tipo.id);
                        comp.setComplaintTypeName(item.tipo.tipo);
                        comp.setComplaintCurrentStatus(item.status.id);
                        this.apiService.getComplaintFiles(comp).subscribe(
                            data => {
                                for (var d of data) {
                                    if (d.tipo === this.IMAGE) {
                                        comp.addComplaintPhoto(this.IMAGE);
                                    } else if (d.tipo === this.VIDEO) {
                                        comp.setComplaintVideo(this.VIDEO);
                                    } else if (d.tipo === this.AUDIO) {
                                        comp.setComplaintAudio(this.AUDIO);
                                    }
                                }
                            },
                            error => {
                                console.log(JSON.stringify(error));
                            },
                            () => { }
                        );
                        tmp_list_of_complaints.push(comp);
                    }
            }, error => { },
            () => {
                this.list_of_complaints = tmp_list_of_complaints.reverse();
                MyApp.prototype.total_complaints = this.list_of_complaints.length;
                loader.dismiss();
            }
        );
    }

    createComplaint() {
        this.navCtrl.push(ComplaintPage)
    }

    openComplaint(comp: Complaint) {
        this.navCtrl.push(ComplaintReviewPage, { complaint: comp });
    }

}
