import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@Component({
    selector: 'page-page2',
    templateUrl: 'page2.html'
})
export class Page2 {

    constructor(public navCtrl: NavController, public navParams: NavParams, private iab: InAppBrowser) { }

    openBrowserOn(url: string) {
        if (url === "Facebook") {
            this.iab.create('https://www.facebook.com/AlcaldiaASDERD/');
        } else if (url === "Twitter") {
            this.iab.create('https://twitter.com/search?q=alcaldiaasdeRD');
        } else if (url === "Instagram") {
            this.iab.create('http://instagram.com/AlcaldiaASDE');
        }
    }
}
