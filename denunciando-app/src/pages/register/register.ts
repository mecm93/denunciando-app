import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { TermsPage } from '../terms/terms';

/*
  Generated class for the Register page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'page-register',
    templateUrl: 'register.html'
})
export class RegisterPage {

    public register_form: FormGroup;
    public pw_validation: boolean = false;
    public submit_attempt: boolean = false;

    constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public formBuilder: FormBuilder) {
        this.register_form = this.formBuilder.group({
            first_name: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z\xD1\xF1 ]*'), Validators.required])],
            last_name: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z\xD1\xF1 ]*'), Validators.required])],
            email: ['', Validators.compose([Validators.maxLength(40), Validators.pattern('[a-zA-Z0-9\xD1\xF1.!#$%&�*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*'), Validators.required])],
            password: ['', Validators.compose([Validators.minLength(8), Validators.required])],
            re_password: ['', Validators.compose([Validators.minLength(8), Validators.required])]
        });
    }

    ionViewDidLoad() { }

    continueRegistration() {
        this.submit_attempt = true;
        if (this.register_form.valid)
            if (this.register_form.value.password !== this.register_form.value.re_password) {
                this.pw_validation = false;
            }
            else {
                this.pw_validation = true;
                this.navCtrl.push(TermsPage, {
                    first_name: this.register_form.value.first_name,
                    last_name: this.register_form.value.last_name,
                    email: this.register_form.value.email,
                    pw: this.register_form.value.password
                });
            }
    }

}
