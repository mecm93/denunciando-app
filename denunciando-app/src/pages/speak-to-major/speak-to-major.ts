import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { Message } from '../../providers/message';
/*
  Generated class for the SpeakToMajor page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

@Component({
    selector: 'page-speak-to-major',
    templateUrl: 'speak-to-major.html'
})
export class SpeakToMajorPage {

    constructor(public navCtrl: NavController, public navParams: NavParams, private message: Message) {
    }

    ionViewDidLoad() { }

    sendForm() {
        // TODO: Validate Internet Connection
        this.message.showAlert('Verifique su conexi&#243;n a internet e intente luego');
    }

    getCommentsLength(text: string): number {
        if (text !== undefined)
            return 500 - text.length;
        else
            return 500;
    }

}
