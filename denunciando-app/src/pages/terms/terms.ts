import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';

import { LoginPage } from '../login/login';
import { User } from '../../providers/user';
import { Message } from '../../providers/message';
import { ApiService } from '../../providers/api-service';

/*
  Generated class for the Terms page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'page-terms',
    templateUrl: 'terms.html'
})
export class TermsPage {

    constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
        private loadingCtrl: LoadingController, private message: Message, public apiService: ApiService) {
    }

    ionViewDidLoad() { }

    register(terms) {
        if (terms) {
            let loader = this.loadingCtrl.create({
                content: "Registrando..."
            });
            loader.present();

            setTimeout(() => {
                this.apiService.registerUser(this.navParams.data.first_name, this.navParams.data.last_name, this.navParams.data.email, this.navParams.data.pw).subscribe(
                    data => {
                        this.message.showToast("Usuario creado satisfactoriamente");
                    }, error => {
                        this.message.showCustomAlert('Advertencia', 'Hubo un error creando este usuario, por favor verificar e intentar luego.');
                    }, () => {
                        this.navCtrl.setRoot(LoginPage);
                        loader.dismissAll();
                    }
                );
            }, 1000);

        } else {
            this.message.showCustomAlert("Advertencia", "Debe de aceptar los terminos para finalizar el proceso de regristaci&#243;n.");
        }
    }
}
