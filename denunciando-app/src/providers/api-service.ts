import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Http, Headers, Response, RequestOptions, Request } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Storage } from '@ionic/storage';
import { MediaFile } from '@ionic-native/media-capture';
import { User } from './user';
import { Message } from './message';
import { Complaint } from './complaint';
import { GlobalData } from './global-data';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { File } from '@ionic-native/file';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

declare var cordova: any;
/*
  Generated class for the AuthService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ApiService {

    // APP USER
    private role: number = 1;
    private user: User = new User();
    private global_data = new GlobalData();

    constructor(public http: Http, private storage: Storage, private transfer: Transfer, private message: Message, private file: File, private platform: Platform) { }

    registerUser(f_name: string, l_name: string, e: string, p: string): any {
        let body = {
            "nombres": f_name, "apellidos": l_name, "correoElectronico": e,
            "claveAcceso": p, "roleId": this.role
        };
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post("http://52.24.250.76:8080/appdenuncias/usuario", JSON.stringify(body), { headers: headers }).map(
            (response: Response) => response.json()
        );
    }

    loginUser(e: string, p: string): any {
        let body = 'email=' + e + '&clave=' + p;
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        return this.http.post("http://52.24.250.76:8080/appdenuncias/usuario/login", body, new RequestOptions({ headers: headers })).map(
            (response: Response) => response.json()
        );
    }

    logoutUser() {
        // Remove data
        this.user = null;
        this.storage.remove('user_id');
    }

    recoverPassword(e: string) {
        let body = 'email=' + e;
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        return this.http.post("http://52.24.250.76:8080/appdenuncias/usuario/recover", body, new RequestOptions({ headers: headers })).map(
            (response: Response) => response
        );
    }


    getComplaints(): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.getUserId().flatMap(data => {
            return this.http.get("http://52.24.250.76:8080/appdenuncias/denuncia/usuario/" + this.user.getUserId(), { headers: headers }).map(
                (response: Response) => response.json()
            );
        });
    }

    getComplaintFiles(c: Complaint): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.get("http://52.24.250.76:8080/appdenuncias/denuncia/" + c.getComplaintId() + "/anexo", { headers: headers }).map(
            (response: Response) => response.json()
        );
    }

    createComplaint(c: Complaint): any {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let body = {
            "direccion": c.getComplaintMapAddress(), "descripcion": c.getComplaintDescription(),
            "latitud": c.getComplaintMapLatLng().lat(), "longitud": c.getComplaintMapLatLng().lng(),
            "usuarioId": this.user.getUserId(), "tipoId": c.getComplaintTypeIndex()
        };
        return this.http.post("http://52.24.250.76:8080/appdenuncias/denuncia", body, { headers: headers }).map(
            (response: Response) => response.json()
        );
    }

    addAttachments(c: Complaint) {
        var url = "http://52.24.250.76:8080/appdenuncias/denuncia/" + c.getComplaintId() + "/anexo";
        var targetPaths = [];
        var filenames = [];
        if (c.getComplaintPhotos() !== null)
            for (let image of c.getComplaintPhotos()) {
                targetPaths.push(this.pathForFile(image));
                filenames.push(image);
            }

        if (c.getComplaintAudio() !== null) {
            targetPaths.push(this.pathForFile(c.getComplaintAudio()));
            filenames.push(c.getComplaintAudio());
        }

        if (c.getComplaintVideo() !== null) {
            targetPaths.push(this.pathForFile(c.getComplaintVideo()));
            filenames.push(c.getComplaintVideo());
        }

        const fileTransfer: TransferObject = this.transfer.create();

        for (let index = 0; index < targetPaths.length; ++index) {
            var options = {
                fileKey: "file",
                fileName: filenames[index],
                chunkedMode: false,
                mimeType: "multipart/form-data",
                params: { 'fileName': filenames[index] }
            };

            fileTransfer.upload(targetPaths[index], url, options).then(
                data => { },
                err => {
                    this.message.showAlert(JSON.stringify(err) + "; " + filenames[index]);
                }
            );
        }
    }

    public pathForFile(file: string) {
        if ((file.indexOf("mp4") > 0 || file.indexOf("m4a") > 0) && file.indexOf("/") > 0) {
            this.message.showAlert(this.file.externalRootDirectory + file);
            return this.file.externalRootDirectory + file;
        }
        return this.file.dataDirectory + file;
    }

    // ============= USER =============
    getUserId(): Observable<number> {
        console.log(JSON.stringify(this.storage.get('user_id')));
        return Observable.fromPromise(this.storage.get('user_id'));
    }

    setCurrentUser(user: any) {
        this.storage.set('user_id', user.id);
        this.user.setUserId(user.id);
        this.user.setUserEmail(user.correoElectronico);
        this.user.setUserFirstName(user.nombres);
        this.user.setUserLastName(user.apellidos);
    }

    getCurrentUser(): User {
        return this.user;
    }
    // ================================
}