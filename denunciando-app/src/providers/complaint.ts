import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { MediaFile } from '@ionic-native/media-capture';
import 'rxjs/add/operator/map';

/*
  Generated class for the Complaint provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/

// ALL RELATABLE CODE FOR A COMPLAINT
@Injectable()
export class Complaint {

    private complaint_current_status = -1;
    private complaint_status: Array<{ id: number, title: string, color: string }> = [
        { id: 1, title: "Pendiente", color: "danger" },
        { id: 2, title: "En Proceso", color: "blue" },
        { id: 3, title: "No Aceptada", color: "danger" },
        { id: 4, title: "En Proceso", color: "blue" },
        { id: 5, title: "Completada", color: "secondary" }
    ];
    private months: Array<{ index: number, name: string }> = [
        { index: 1, name: "Enero" },
        { index: 2, name: "Febreo" },
        { index: 3, name: "Marzo" },
        { index: 4, name: "Abril" },
        { index: 5, name: "Mayo" },
        { index: 6, name: "Junio" },
        { index: 7, name: "Julio" },
        { index: 8, name: "Agosto" },
        { index: 9, name: "Septiembre" },
        { index: 10, name: "Octubre" },
        { index: 11, name: "Noviembre" },
        { index: 12, name: "Diciembre" }
    ];

    private complaint_id: number = Math.floor(Math.random() * (99999 - 10000)) + 10000;
    private complaint_date: Date;
    private complaint_type_index: number;
    private complaint_type_name: string;
    private complaint_description: string;
    private complaint_map_latitude: number;
    private complaint_map_longitude: number;
    private complaint_map_latLng: any;
    private complaint_map_address: string;

    //PHOTOS MAX = 3
    private complaint_photos_index: number;
    private complaint_photos: Array<string> = [];
    private complaint_video: string;
    private complaint_audio: string;

    constructor(public http?: Http) {
        this.complaint_photos_index = 0;
        this.complaint_video = null;
        this.complaint_audio = null;
    }

    setComplaintCurrentStatus(n: number) {
        this.complaint_current_status = n;
    }

    getComplaintCurrentStatus(): number {
        return this.complaint_current_status;
    }

    getComplaintId(): number {
        return this.complaint_id;
    }

    setComplaintId(id: number) {
        this.complaint_id = id;
    }

    getMonthByIndex(index: number): string {
        return this.months[index].name;
    }

    setComplaintDate(date: string) {
        this.complaint_date = new Date(date);
    }

    getComplaintDateFormatted(): string {
        return this.complaint_date.getDate() + " " + this.getMonthByIndex(this.complaint_date.getMonth()) + " " + this.complaint_date.getFullYear();
    }

    getComplaintStatus(): Array<{ id: number, title: string, color: string }> {
        return this.complaint_status;
    }

    getComplaintStatusByIndex(index: number): { id: number, title: string, color: string } {
        return this.complaint_status[index - 1];
    }

    getComplaintStatusTitleByIndex(index: number): string {
        return this.complaint_status[index - 1].title;
    }

    getComplaintStatusIndexByTitle(title: string): number {
        var index: number = -1;
        for (let status of this.complaint_status) {
            if (status.title === title)
                index = status.id;
        }

        return index;
    }

    getComplaintStatusColorByIndex(index: number): string {
        return this.complaint_status[index - 1].color;
    }

    getComplaintTypeIndex(): number {
        return this.complaint_type_index;
    }

    setComplaintTypeIndex(n: number) {
        this.complaint_type_index = n;
    }

    getComplaintTypeName(): string {
        return this.complaint_type_name;
    }

    setComplaintTypeName(n: string) {
        this.complaint_type_name = n;
    }

    getComplaintDescription(): string {
        return this.complaint_description;
    }

    setComplaintDescription(d: string) {
        this.complaint_description = d;
    }

    setComplaintMapLatLng(o: any) {
        this.complaint_map_latitude = o.lat();
        this.complaint_map_longitude = o.lng();
        this.complaint_map_latLng = o;
    }

    getComplaintMapLatLng(): any {
        if (this.complaint_map_latLng !== null || this.complaint_map_latLng !== undefined)
            return this.complaint_map_latLng;

        return this.complaint_map_latitude + ", " + this.complaint_map_longitude;
    }

    getComplaintMapLat(): number {
        return this.complaint_map_latitude;
    }

    setComplaintMapLat(l: number) {
        this.complaint_map_latitude = l;
    }

    getComplaintMapLng(): number {
        return this.complaint_map_longitude;
    }

    setComplaintMapLng(l: number) {
        this.complaint_map_longitude = l;
    }

    getComplaintMapAddress(): string {
        return this.complaint_map_address;
    }

    setComplaintMapAddress(a: string) {
        this.complaint_map_address = a;
    }

    getComplaintPhotoIndex(): number {
        return this.complaint_photos_index;
    }

    setComplaintPhotoIndex(i: number) {
        this.complaint_photos_index = i;
    }

    getComplaintPhotoByIndex(index: number): string {
        return this.complaint_photos[index];
    }

    getComplaintPhotos(): string[] {
        return this.complaint_photos;
    }

    setComplaintPhotos(p: string[]) {
        if (p.length - 1 < 3) {
            this.complaint_photos = p;
            this.complaint_photos_index = p.length - 1;
        } else
            console.log("Max photos number passed!")
    }

    addComplaintPhoto(p: string) {
        if (this.complaint_photos_index < 3) {
            this.complaint_photos.push(p);
            ++this.complaint_photos_index;
        } else
            console.log("Max photos number reached!");
    }

    removeComplaintPhotoByIndex(index: number) {
        // TODO: Check if this is setting the data to correct indexes
        --this.complaint_photos_index;

        if (index === 2)
            this.complaint_photos.splice(index);
        else if (index === 1) {
            if (this.complaint_photos[2] !== null) {
                this.complaint_photos[index] = this.complaint_photos[2];
                this.complaint_photos.splice(2);
            } else
                this.complaint_photos.splice(index);
        }
        else if (index === 0) {
            if (this.complaint_photos[2] !== null && this.complaint_photos[1] !== null) {
                this.complaint_photos[index] = this.complaint_photos[1];
                this.complaint_photos[1] = this.complaint_photos[2];
                this.complaint_photos.splice(2);
            } else if (this.complaint_photos[1] !== null) {
                this.complaint_photos[index] = this.complaint_photos[1];
                this.complaint_photos.splice(1);
            } else
                this.complaint_photos.splice(index);
        }
    }

    getComplaintVideo(): string {
        return this.complaint_video
    }

    setComplaintVideo(v: string) {
        this.complaint_video = v;
    }

    getComplaintAudio(): string {
        return this.complaint_audio;
    }

    setComplaintAudio(a: string) {
        this.complaint_audio = a;
    }
}
