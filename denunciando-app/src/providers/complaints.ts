import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Complaint } from './complaint';

/*
  Generated class for the Complaints provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/

// THIS CLASS CONTAINS THE WHOLE LIST OF COMPLAINTS
// FROM SERVER AND LOCAL

@Injectable()
export class Complaints {

    private static complaints: Array<Complaint> = [];

    constructor(public http?: Http) {
    }

    getComplaintByIndex(index: number): Complaint {
        return Complaints.complaints[index];
    }

    getComplaints(): Complaint[] {
        return Complaints.complaints;
    }

    addComplaint(complaint: Complaint) {
        Complaints.complaints.push(complaint);
    }

    getComplaintsIndex(): number {
        if (Complaints.complaints !== undefined)
            return Complaints.complaints.length;

        return 0;
    }


}
