import { Injectable } from '@angular/core';

@Injectable()
export class GlobalData {

    // PARENT
    public SERVER_URL: string = "http://52.24.250.76:8080/appdenuncias/";

    // CHILDS
    public SERVER_URL_COMPLAINT: string = "http://52.24.250.76:8080/appdenuncias/denuncia";
    public SERVER_URL_ADD_ATTACHMENT_COMPLAINT: string = "http://52.24.250.76:8080/appdenuncias/denuncia/id/anexo";

    public SERVER_URL_USER: string = "http://52.24.250.76:8080/appdenuncias/usuario";
    public SERVER_URL_USER_COMPLAINT: string = "http://52.24.250.76:8080/appdenuncias/denuncia/usuario/id";

    // GOOGLE MAPS API KEY
    public API_KEY: string = "AIzaSyCy_HEno77yLgMwmZROFHexXPw2pqkXWN8";

    constructor() { }
    
}