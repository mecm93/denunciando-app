import { Injectable, ElementRef} from '@angular/core';
import { LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { Message } from './message';
import { Complaint } from './complaint';

/*
  Generated class for the Map provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/

declare var google;

@Injectable()
export class Map {
    
    private lat: number = 18.505439;
    private lng: number = -69.811439;
    private zoom: number = 12;
    private map: any;
    private complaint: Complaint;
    private marker_visible: boolean;
    private previous_marker_on_map: any;

    constructor(public message: Message, public loadingCtrl: LoadingController, public http?: Http) {
        this.complaint = new Complaint();
        this.marker_visible = false;
    }

    // Load the map and validates if click event is necessary
    loadMap(e: ElementRef, lat?: number, lng?: number, zoom?: number) {
        let loading = this.loadingCtrl.create({
            content: 'Cargando...'
        });

        loading.present();
        if(lat !== undefined)
            this.lat = lat;
        if(lng !== undefined) 
            this.lng = lng;
        if(zoom !== undefined)
            this.zoom = zoom;
            
        let latLng = new google.maps.LatLng(this.lat, this.lng);

        let mapOptions = {
            center: latLng,
            zoom: this.zoom,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true
        }
        
        this.map = new google.maps.Map(e, mapOptions);
        
        google.maps.event.addListenerOnce(this.map, 'idle', () => {
           setTimeout(() => {
               loading.dismiss();
           }, 100);
        });
    }

    // Validates if clicked area is on Santo Domingo Este
    // Remove previous and add new marker on Map
    // Returns Complaint object with data
    addMarker(): Complaint {
        this.complaint = new Complaint();
        google.maps.event.addListener(this.map, 'click', (event) => {
            var found = false;
            var latLng = event.latLng;
            var geocoder = new google.maps.Geocoder;
            geocoder.geocode({ 'location': event.latLng }, (results, status) => {
                for (let i of [1, 2, 3, 4, 5, 6, 7, 8, 9])
                    if (results[0] !== undefined &&
                        results[0].address_components[i] !== undefined &&
                        results[0].address_components[i].long_name !== undefined)
                        if (results[0].address_components[i].long_name == "Santo Domingo Este") {
                            if (status === 'OK') {
                                found = true;
                                break;
                            }
                        }


                if (!found)
                    this.message.showAlert("La ubicacion esta fuera de Santo Domingo Este");
                else {
                    if (this.marker_visible) {
                        //Remove previous marker from map
                        this.previous_marker_on_map.setMap(null);

                        this.marker_visible = false;
                    }

                    var marker = new google.maps.Marker({
                        map: this.map,
                        position: latLng,
                        title: 'Denuncia'
                    });
                    
                    this.previous_marker_on_map = marker;
                    this.marker_visible = true;
                    this.complaint.setComplaintMapLatLng(latLng);
                    this.complaint.setComplaintMapAddress(results[0].formatted_address.replace("Dominican Republic", "Republica Dominicana"));
                    
                }
            });

        });

        return this.complaint;
    }
}
