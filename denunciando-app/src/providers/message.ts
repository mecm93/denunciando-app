import { Injectable } from '@angular/core';
import { ToastController, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';



/*
  Generated class for the Message provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Message {

    constructor(public toastCtrl: ToastController, public alertCtrl: AlertController, public http?: Http ) { }

    showToast(message: string) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 7000,
            position: 'bottom',
            cssClass: 'custom-toast'
        });
        toast.present();
    }

    showAlert(message: string) {
        let toast = this.alertCtrl.create({
            title: 'Error',
            subTitle: message,
            buttons: ['OK']
        });
        toast.present();
    }

    showCustomAlert(title: string, message: string) {
        let toast = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['OK']
        });
        toast.present();
    }

}
