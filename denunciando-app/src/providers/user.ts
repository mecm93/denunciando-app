import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

/*
  Generated class for the User provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/

//ALL RELATABLE DATA OF A USER

@Injectable()
export class User {

    private user_id: number;
    private user_first_name: string;
    private user_last_name: string;
    private user_email: string;
    private user_password: string;
    private user_phone: string;

    constructor() { }


    getUserId() {
        return this.user_id;
    }

    setUserId(id: number) {
        this.user_id = id;
    }
  getUserFullName() {
      return this.user_first_name + " " + this.user_last_name;
  }

  getUserFirstName() {
      return this.user_first_name;
  }

  setUserFirstName(f: string) {
      this.user_first_name = f;
  }

  getUserLastName() {
      return this.user_last_name;
  }

  setUserLastName(l: string) {
      this.user_last_name = l;
  }

  getUserEmail() {
      return this.user_email;
  }

  setUserEmail(e: string) {
      this.user_email = e;
  }

  getUserPassword() {
      return this.user_password;
  }

  setUserPassword(p: string) {
      this.user_password = p;
  }

  getUserPhone() {
      return this.user_phone;
  }

  setUserPhone(p: string) {
      this.user_phone = p;
  }


}
